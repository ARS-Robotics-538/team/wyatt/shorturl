#!/bin/bash

#conf=`realpath ${1}`
outdir=`realpath ${1}`

help()
{
	echo "Usage: ${1} <outdir> < <indexfile>"
	echo ""
	echo "Generates shorturl redirection files in outdir based on an indexfile read from stdin."
	echo "Ex: ${1} outdir < index.txt"
	echo ""
	echo -e "Index file is a tab-seperated text file following the format 'ID\tUrl'."
	echo -e "Ex: archwiki\thttps://wiki.archlinux.org"
	exit
}
#if [ -z ${1} ]#| ${1} -eq "-h" | ${1} -eq "--help" ]
if [ -z "${1}" ]; then help; fi
if [ "${1}" == "--help" ]; then help; fi
if [ "${1}" == "-h" ]; then help; fi

cd "${outdir}"
while read -t 0.1 line
do
	
	id=`echo ${line} | awk '{print $1}'`
	url=`echo ${line} | awk '{print $2}'`

	cat << _EOF_ > ${id}.html
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Redirecting</title>
	<meta http-equiv="Refresh" content="5;'${url}'"/>
</head>
<body>
	<p>
		Redirecting to <a href="${url}">${url}</a> in 5 seconds.
	</p>
</body>
</html>
_EOF_

done
